// +build terraform_0.15.3

package plancomponents

import (
	"fmt"

	"github.com/hashicorp/terraform/backend/remote-state/http"
	"github.com/hashicorp/terraform/configs/configload"
	"github.com/hashicorp/terraform/plans"
	"github.com/hashicorp/terraform/plans/planfile"
	"github.com/hashicorp/terraform/states/statefile"
	"github.com/zclconf/go-cty/cty"
)

// PlanComponents struct
type PlanComponents struct {
	Plan  *plans.Plan
	State *statefile.File
	PrevState *statefile.File
	Snap  *configload.Snapshot
}

// New PlanComponents
func New(r *planfile.Reader) (*PlanComponents, error) {
	planIn, err := r.ReadPlan()
	if err != nil {
		return nil, fmt.Errorf("failed to read plan: %w", err)
	}

	stateFileIn, err := r.ReadStateFile()
	if err != nil {
		return nil, fmt.Errorf("failed to read state: %w", err)
	}

	prevStateFileIn, err := r.ReadPrevStateFile()
	if err != nil {
		return nil, fmt.Errorf("failed to read previous state: %w", err)
	}

	snapIn, err := r.ReadConfigSnapshot()
	if err != nil {
		return nil, fmt.Errorf("failed to read config snapshot: %w", err)
	}

	return &PlanComponents{
		Plan:  planIn,
		State: stateFileIn,
		PrevState: prevStateFileIn,
		Snap:  snapIn,
	}, nil
}

// FromFile function
func FromFile(fn string) (*PlanComponents, error) {
	pf, err := planfile.Open(fn)
	if err != nil {
		return nil, fmt.Errorf("failed to open plan file for reading: %w", err)
	}

	pc, err := New(pf)
	if err != nil {
		return nil, fmt.Errorf("failed to extract plan components: %w", err)
	}

	return pc, nil
}

// Get function
func (pc *PlanComponents) Get(key string) (string, error) {
	if pc.Plan.Backend.Type != "http" {
		return "", fmt.Errorf("unsupported backend type: %w", pc.Plan.Backend.Type)
	}

	backend := http.New()
	schema := backend.ConfigSchema()

	decodedConfig, err := pc.Plan.Backend.Config.Decode(schema.ImpliedType())
	if err != nil {
		return "", fmt.Errorf("failed to decode backend config: %w", err)
	}

	configValueMap := decodedConfig.AsValueMap()

	value, found := configValueMap[key]
	if !found || value.IsNull() {
		return "", nil
	}

	return value.AsString(), nil
}

// Set function
func (pc *PlanComponents) Set(key string, value string) error {
	if pc.Plan.Backend.Type != "http" {
		return fmt.Errorf("unsupported backend type: %s", pc.Plan.Backend.Type)
	}

	backend := http.New()
	schema := backend.ConfigSchema()

	decodedConfig, err := pc.Plan.Backend.Config.Decode(schema.ImpliedType())
	if err != nil {
		return fmt.Errorf("failed to decode backend config: %w", err)
	}

	configValueMap := decodedConfig.AsValueMap()

	configValueMap[key] = cty.StringVal(value)

	config, err := plans.NewDynamicValue(cty.ObjectVal(configValueMap), schema.ImpliedType())
	if err != nil {
		return fmt.Errorf("failed to modify backend config: %w", err)
	}

	pc.Plan.Backend.Config = config
	return nil
}

// Write PlanComponents to a file
func (pc *PlanComponents) Write(planFn string) error {
	err := planfile.Create(planFn, pc.Snap, pc.PrevState, pc.State, pc.Plan)
	if err != nil {
		return fmt.Errorf("failed to write plan file: %w", err)
	}
	return nil
}
