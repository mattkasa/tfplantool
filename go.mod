module gitlab.com/mattkasa/tfplantool

go 1.16

require (
	github.com/apparentlymart/go-textseg/v12 v12.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/hashicorp/terraform v0.15.3
	github.com/urfave/cli/v2 v2.2.0
	github.com/zclconf/go-cty v1.8.3
)
